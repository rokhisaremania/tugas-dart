void main(List<String> args) async {
  print("Ready ?");
  print(await Line());
  print(await Line1());
  print(await Line2());
  print(await Lirik1());
  print(await Lirik2());
  print(await Lirik3());
  print(await Lirik4());
  print(await Lirik5());
  print(await Lirik6());
  print(await Lirik7());
  print(await Lirik8());
}

Future<String> Line() async {
  String intro = "1";
  return await Future.delayed(Duration(seconds: 2), () => (intro));
}

Future<String> Line1() async {
  String intro = "2";
  return await Future.delayed(Duration(seconds: 2), () => (intro));
}

Future<String> Line2() async {
  String intro = "3";
  return await Future.delayed(Duration(seconds: 2), () => (intro));
}

Future<String> Lirik1() async {
  String lirik = "Piye... seng njelaske karo wong tuwo";
  return await Future.delayed(Duration(seconds: 1), () => (lirik));
}

Future<String> Lirik2() async {
  String lirik = "Wis nglakoni tekan semene";
  return await Future.delayed(Duration(seconds: 10), () => (lirik));
}

Future<String> Lirik3() async {
  String lirik = "Nek akhire bakal bubar pisahan";
  return await Future.delayed(Duration(seconds: 6), () => (lirik));
}

Future<String> Lirik4() async {
  String lirik = "Kowe kegudo tresno karo wong liyo";
  return await Future.delayed(Duration(seconds: 7), () => (lirik));
}

Future<String> Lirik5() async {
  String lirik = "Ngelali... Kowe sing tau omong dewe";
  return await Future.delayed(Duration(seconds: 8), () => (lirik));
}

Future<String> Lirik6() async {
  String lirik = "Nglakoni tresno tekan tuwo";
  return await Future.delayed(Duration(seconds: 6), () => (lirik));
}

Future<String> Lirik7() async {
  String lirik = "Ora ngeliyo mung aku ning atimu";
  return await Future.delayed(Duration(seconds: 8), () => (lirik));
}

Future<String> Lirik8() async {
  String lirik = "Nanging saiki atimu ono wong liyo";
  return await Future.delayed(Duration(seconds: 8), () => (lirik));
}

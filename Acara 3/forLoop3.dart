void main(List<String> args) {
  for (var deret = 0; deret < 10; deret += 2) {
    print("Iterasi dengan increment counter 2 : " + deret.toString());
  }
  print("=================================");
  for (var num = 15; num > 0; num -= 3) {
    print("Iterasi dengan decrement counter 3 : " + num.toString());
  }
}

void main(List<String> args) {
  //premis bernilai true
  if (true) {
    print("ini benar");
  }

  //premis bernilai false
  if (false) {
    print("ini tidak jalan");
  }

  //premis perbandingan
  var laptop = 'Asus';
  if (laptop == 'Asus') {
    print("Laptop anda bermerk ${laptop}");
  }
}
